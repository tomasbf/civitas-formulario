# Civitas Formulario



## Getting started

To make it easy for you to get started with GitLab, here's a list of recommended next steps.

Already a pro? Just edit this README.md and make it your own. Want to make it easy? [Use the template at the bottom](#editing-this-readme)!

## Add your files

- [ ] [Create](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#create-a-file) or [upload](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#upload-a-file) files
- [ ] [Add files using the command line](https://docs.gitlab.com/ee/gitlab-basics/add-file.html#add-a-file-using-the-command-line) or push an existing Git repository with the following command:

```
cd existing_repo
git remote add origin https://gitlab.com/tomasbf/civitas-formulario.git
git branch -M main
git push -uf origin main
```


## Name
Site do civitas com formulário

## Description
Cadastre usuários e poste serviços e produtos


## Usage
Abra o terminal na pagina do projeto e rode "npx json-server --watch db.json" no terminal para rodar o arquivo json em um server local.
Verifique se o server está em localhost:3000.

Abra o Live Server do vsCode para visualizar os arquivos html (index.html é a página home) e navegue pelo site


## Authors and acknowledgment
Tomás Barboza de Farias
