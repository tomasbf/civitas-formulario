import checarBotao from "../utils/checarBotao.js";
import alertaErro from "../utils/checarSenha.js";
import popupMsg from "../utils/popupMsg.js";
import criarRegistro from "./criarRegistro.js";
import getPessoa from "./getPessoa.js";
import checarInputVazio from "../utils/checarInputVazio.js";

const nomeJuridicaInput = document.querySelector("#nome_empresa");
const cnpjInput = document.querySelector("#cnpj");
const cepInput = document.querySelector("#cep");
const emailJuridicaInput = document.querySelector("#email_empresa");
const senhaJuridicaInput = document.querySelector("#senha_pessoa_juridica");
const numeroInput = document.querySelector("#numero");
const complementoInput = document.querySelector("#complemento");
const nomeRepresentanteInput = document.querySelector("#nome_representante");
const formJuridica = document.querySelector(".pessoa_juridica");

const nomeFisicaInput = document.querySelector("#nome");
const dataNascimentoInput = document.querySelector("#data_nascimento");
const cpfInput = document.querySelector("#cpf");
const emailFisicaInput = document.querySelector("#email");
const senhaFisicaInput = document.querySelector("#senha_pessoa_fisica");
const formFisica = document.querySelector(".pessoa_fisica");

const botaoFisica = document.querySelector("#escolher_fisica");
const botaoJuridica = document.querySelector("#escolher_juridica");

const cadastrarFisicaBtn = document.querySelector(".pessoa_fisica button");
const cadastrarJuridicaBtn = document.querySelector(".pessoa_juridica button");

cadastrarFisicaBtn.setAttribute("value", "fisicas");
cadastrarJuridicaBtn.setAttribute("value", "juridicas");



const params = (new URL(document.location.href)).searchParams;
const emailPassado = params.get('email');

if(emailPassado !== null) {
    emailFisicaInput.setAttribute("value", emailPassado);
    emailJuridicaInput.setAttribute("value", emailPassado);
}



let cadastrarBtn = cadastrarFisicaBtn;

let fisicaOuJuridica = checarBotao(botaoFisica, "fisicas", botaoJuridica, "juridicas", "#escolher_tipo_usuario", "Escolha um tipo de pessoa");



const registrarPessoa = async (e) => {
    e.preventDefault();

    const fisicaOuJuridica = cadastrarBtn.value;

    let pessoa;

    switch (fisicaOuJuridica) {
        case "fisicas":

            const nomeFisica = nomeFisicaInput.value;
            const dataNascimento = dataNascimentoInput.value;
            const cpf = cpfInput.value;
            const emailFisica = emailFisicaInput.value;
            const senhaFisica = senhaFisicaInput.value;



            // Colocar todas as condições
            if (
                await alertaErro(senhaFisicaInput) &&
                ! await checarInputVazio("nome", "Digite seu nome") &&
                ! await checarInputVazio("data_nascimento", "Coloque a sua data de nascimento") &&
                ! await checarInputVazio("cpf", "Insira seu CPF") &&
                ! await checarInputVazio("email", "Insira seu Email")

            ) {

                if (await getPessoa(emailFisica, null) !== null) {
                    popupMsg(`label[for=\"email\"]`, "Email já em uso");
                    return false;
                }

                pessoa = {

                    nome: nomeFisica,
                    dataNascimento: dataNascimento,
                    cpf: cpf,
                    email: emailFisica,
                    senha: senhaFisica


                };
            } else {
                return false;
            }


            break;


        case "juridicas":


            const nomeJuridica = nomeJuridicaInput.value;
            const cnpj = cnpjInput.value;
            const cep = cepInput.value;
            const numero = numeroInput.value;
            const complemento = complementoInput.value;
            const nomeRepresentante = nomeRepresentanteInput.value;


            const emailJuridica = emailJuridicaInput.value;
            const senhaJuridica = senhaJuridicaInput.value;


            // Colocar todas as condições
            if (
                await alertaErro(senhaJuridicaInput) &&
                ! await checarInputVazio("nome_empresa", "Insira o nome da empresa") &&
                ! await checarInputVazio("cnpj", "Insira o CNPJ da empresa") &&
                ! await checarInputVazio("cep", "Insira o CEP") &&

                ! await checarInputVazio("complemento", "Insira o complemento do endereço") &&
                ! await checarInputVazio("numero", "Insira o número do prédio") &&
                ! await checarInputVazio("nome_representante", "Insira o nome do representante") &&

                ! await checarInputVazio("email_empresa", "Insira um email de contato")

            ) {


                if (await getPessoa(emailJuridica, null) !== null) {

                    popupMsg(`label[for=\"email_empresa\"]`, "Email já em uso");
                    return false;
                }

                pessoa = {

                    nome: nomeJuridica,
                    cnpj: cnpj,
                    cep: cep,
                    complemento: complemento,
                    numero: numero,
                    nome_representante: nomeRepresentante,
                    email: emailJuridica,
                    senha: senhaJuridica

                };

            } else {
                return false;
            }


            break;

    }


    console.log(await criarRegistro(pessoa, fisicaOuJuridica));
    window.location.href = './entrar.html';

    return false;
}

cadastrarFisicaBtn.addEventListener("click", async(e) => {
    await registrarPessoa(e);
});
cadastrarJuridicaBtn.addEventListener("click", async(e) => {
    await registrarPessoa(e);
});


document.addEventListener("click", (e) => {

    fisicaOuJuridica = checarBotao(botaoFisica, "fisicas", botaoJuridica, "juridicas", "#escolher_tipo_usuario", "Escolha um tipo de pessoa");

    if (fisicaOuJuridica === "fisicas") {
        cadastrarBtn = cadastrarFisicaBtn;
    }
    else if (fisicaOuJuridica === "juridicas") {
        cadastrarBtn = cadastrarJuridicaBtn;
    }
    
    

});


