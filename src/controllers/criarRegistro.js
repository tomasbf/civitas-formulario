export default async function criarRegistro(conjuntoDados, qual) {
    try {

        const response = await fetch(`http://localhost:3000/${qual}`, {
            method: 'POST',
            headers: {
                Accept: "application/json",
                "Content-Type": "application/json"
            },
            body: JSON.stringify(conjuntoDados)
        });
    

        return await response.json();
                

    } catch (error) {
        console.log(error);

    }
}