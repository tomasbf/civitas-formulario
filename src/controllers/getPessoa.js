const getPessoa = async(email, tipo) => {

    if(tipo === null) {
        try {
        
            let fisicas = await fetch(`http://localhost:3000/fisicas`,{
                method: "GET"
                });
            let juridicas = await fetch(`http://localhost:3000/juridicas`,{
                method: "GET"
                });


            
            fisicas = await fisicas.json();
            juridicas = await juridicas.json();
    

            let pessoa = fisicas.filter((elemento) => {
                return (elemento.email === email); 
            })[0];


            let tipo = "fisica";
            if(await pessoa === undefined) {
                pessoa = juridicas.filter((elemento) => {
                    return (elemento.email === email); 
                })[0];
                tipo = "juridica";
            }


            if(pessoa === undefined) return null;

            return [pessoa, tipo];

        } catch (error) {
            console.log(error);
        }
    }

    if(tipo === "fisica" || tipo === "juridica") {
        tipo = tipo.concat("s");
    }

    if(tipo !== "juridicas" && tipo !== "fisicas") return null;
    try {
        
        const response = await fetch(`http://localhost:3000/${tipo}`,{
            method: "GET"
        });

        const content = await response.json();

        const pessoa = content.filter((elemento) => {
            return (elemento.email === email); 
        })[0];



        if(pessoa === undefined) {
            console.log("Erro ao acessar pessoa");
            return null;
        }

        return pessoa;


    } catch (error) {
        console.log(error);
    }

} 
export default getPessoa;