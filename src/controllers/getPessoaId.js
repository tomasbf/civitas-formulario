const getPessoaId = async(id, tipo) => {

    if(tipo === "fisica" || tipo === "juridica") {
        tipo = tipo.concat("s");
    }

    if(tipo !== "fisicas" && tipo !== "juridicas") return null;
    try {
        
        const response = await fetch(`http://localhost:3000/${tipo}`,{
            method: "GET"
        });
        

        const content = await response.json();


        const pessoa = content.filter((elemento) => {
            return (elemento.id == id); 
        })[0];




        if(pessoa === undefined) {
            console.log("Erro ao acessar pessoa");
            return null;
        }

        return pessoa;


    } catch (error) {
        console.log(error);
    }

} 
export default getPessoaId;