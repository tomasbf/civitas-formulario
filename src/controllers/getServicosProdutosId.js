export default async function getServicosProdutosId (id, qual, tipo) {
    try {
        
        if(tipo === "juridica" || tipo === "fisica") tipo = tipo.concat("s");
        if(qual === "servico" || qual === "produto") qual = qual.concat("s");


        const response = await fetch(`http://localhost:3000/${qual}`,{
            method: "GET"
        });
        
        let resultados;
        if(id === null || (tipo !== "juridicas" && tipo !== "fisicas")) {

            resultados = await response.json();
        }
        else {
            resultados = (await response.json()).filter((e) => {
                let tipoPessoa = e.tipoPessoa;
                if(e.tipoPessoa === "fisica" || e.tipoPessoa === "juridica") tipoPessoa = tipoPessoa.concat("s");

                return e.idPessoa === id && tipoPessoa === tipo;

            });
        }

        return resultados;


    }  catch(error) {
        console.log(error);
    }



}