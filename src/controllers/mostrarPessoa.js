import mostrarFisica from "../views/FisicasView.js";
import mostrarJuridica from "../views/JuridicasView.js";

const mostrarPessoa = async (table, id, tipo) => {
    if(tipo === "fisicas" || tipo === "juridicas") tipo = tipo.substring(0, tipo.length - 1);
    if(tipo !== "fisica" && tipo !== "juridica") {
        table.insertAdjacentHTML("beforeend", `<p>Pessoa não encontrada</p>`);
        return false;
    }
    try {
        
        if(id === null) {
            table.insertAdjacentHTML("beforeend", `<p>Pessoa não encontrada</p>`);
            return false;
        }

        const response = await fetch(`http://localhost:3000/${tipo}s`, {
            method: "GET"
        });

        const content = await response.json();
        
        const pessoa = content.filter((elemento) => {
            return (elemento.id == id);
        })[0];

        if(pessoa === null || pessoa === undefined) {
            if(tipo === "fisica")
                table.insertAdjacentHTML("beforeend", `<p>Pessoa não encontrada</p>`);
            else
                table.insertAdjacentHTML("beforeend", `<p>Empresa não encontrada</p>`);

            return false;
        }
        
        switch (tipo) {
            case "fisica":
                table.insertAdjacentHTML("beforeend", mostrarFisica(pessoa));
                break;
            case "juridica":
                table.insertAdjacentHTML("beforeend", mostrarJuridica(pessoa));
                break;
        }

    } catch (error) {
        console.log(error);
    }

}

export default mostrarPessoa;