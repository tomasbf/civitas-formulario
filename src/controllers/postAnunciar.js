import criarRegistro from "./criarRegistro.js";
import popupMsg from "../utils/popupMsg.js";
import checarBotao from "../utils/checarBotao.js";

const botaoServico = document.querySelector("#servico");
const botaoProduto = document.querySelector("#produto");
const botaoCliente = document.querySelector("#cliente");
const botaoAnunciante = document.querySelector("#anunciante");

const descricaoInput = document.querySelector("#descricao");
let precoInput = document.querySelector("#preco");
const tituloInput = document.querySelector("#titulo");

const perfilBtn = document.querySelector(".conta a");


const anunciarBtn = document.querySelector("#anunciar");


const params = (new URL(document.location.href)).searchParams;
const id = params.get('id');
const tipoPessoa = params.get('tipo');


perfilBtn.addEventListener("click", (e) => {
    e.preventDefault();

    document.location.href = `./perfil.html?id=${id}&tipo=${tipoPessoa}`;
});


if(id === null || (tipoPessoa !== "fisica" && tipoPessoa !== "juridica")) {
    document.location.href='./entrar.html';
}


document.addEventListener("click", (e) => {

    if(!anunciarBtn.contains(e.target)) {
        popupMsg( "#tipo" ,"", true);
        popupMsg("#categoria", "", true);
    }


}); 

anunciarBtn.addEventListener("click", (e) => {
    e.preventDefault();

    const tipo = checarBotao(botaoServico, "servicos", botaoProduto, "produtos", "#tipo", "Escolha um tipo");
    if(tipo === false) return false;

    switch(tipo) {
        case "servicos":
            botaoAnunciante.setAttribute("value", "Prestação");
            botaoCliente.setAttribute("value", "Contratação");

            

            break;
        case "produtos":
            botaoAnunciante.setAttribute("value", "Venda");
            botaoCliente.setAttribute("value", "Compra");
            break;
    }

    const categoria = checarBotao(botaoAnunciante, botaoAnunciante.value, botaoCliente, botaoCliente.value, "#categoria", "Escolha uma categoria");
    if(categoria === false) return false;

    precoInput = document.querySelector("#preco");
    const servicoProduto = {

        titulo: tituloInput.value,
        preco: precoInput.value,
        descricao: descricaoInput.value,
        categoria: categoria,
        idPessoa: id,
        tipoPessoa: tipoPessoa

    };

    const resposta = criarRegistro(servicoProduto, tipo);
    return resposta;
    
});




