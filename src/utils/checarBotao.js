
import popupMsg from "./popupMsg.js";
export default function checarBotao(botao1, botao1Valor, botao2, botao2Valor, caminhoPopup, msgPopup) {
    let resultado;
    if(botao1.hasAttribute("checked")) {
        resultado = botao1Valor;
    } else if (botao2.hasAttribute("checked")){
        resultado = botao2Valor;
    }  else {
        
        popupMsg(caminhoPopup, msgPopup);
        return false;
    }
    return resultado;

}