import colocarOutline from "./colocarOutline.js";
import popupMsg from "./popupMsg.js";
import removerOutline from "./removerOutline.js";

export default async function checarInputVazio (qual, msg) {
    const input = document.querySelector(`label[for=\"${qual}\"] input`);

    
    if(input.hasAttribute("required") && (input.value === null || input.value === "")) {
        popupMsg(`label[for=\"${qual}\"]`, msg);

        return true;
    } 
    return false;

}