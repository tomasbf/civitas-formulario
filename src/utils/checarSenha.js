import colocarOutline from "./colocarOutline.js";
import removerOutline from "./removerOutline.js";
import containsNumber from "./containsNumber.js";
import popupMsg from "./popupMsg.js";
import popupRemove from "./popupRemove.js";

const TAM_MIN = 8;

const senhaFisicaInput = document.querySelector("#senha_pessoa_fisica");
const senhaJuridicaInput = document.querySelector("#senha_pessoa_juridica");


const checarSenha = async(senha) => {
    if(senha.length < TAM_MIN) return "pequena";
    if(!containsNumber(senha)) return "numero";

    return "valida";
};



const alertaErro = async (qualSenha) => {
    let caminho;
    switch(qualSenha) {
        case senhaFisicaInput:
            caminho = "senha_pessoa_fisica";
            break;
        case senhaJuridicaInput:
            caminho = "senha_pessoa_juridica";
            break;
        default: 
            const popupFisica = document.querySelector("label[for=\"senha_pessoa_fisica\"] .popuptext");
            const popupJuridica = document.querySelector("label[for=\"senha_pessoa_juridica\"] .popuptext");
            popupRemove(popupFisica, senhaFisicaInput);
            popupRemove(popupJuridica, senhaJuridicaInput);
            return false;
    }

    const statusSenha = await checarSenha(qualSenha.value);

    switch(statusSenha) {
        case "pequena":
            popupMsg(`label[for=\"${caminho}\"]`, "A senha precisa de pelo menos 8 caracteres");

            break;
        case "numero":
            popupMsg(`label[for=\"${caminho}\"]`, "A senha deve conter pelo menos um número");
            break;

        default: 
            
            const popup = document.querySelector(`label[for=\"${caminho}\"] .popuptext`);
            const input = document.querySelectorAll(`label[for=\"${caminho}\"] input`);
            popupRemove(popup, input);
            return true;
    }

    return false;

    
}; 

export default alertaErro;

senhaFisicaInput.addEventListener("blur", async() => {
    await alertaErro(senhaFisicaInput);
});

senhaFisicaInput.addEventListener("keydown", async() => {
    await alertaErro(senhaFisicaInput);
});


senhaJuridicaInput.addEventListener("blur", async() => {
    await alertaErro(senhaJuridicaInput);
});

senhaJuridicaInput.addEventListener("keydown", async() => {
    await alertaErro(senhaJuridicaInput);
});




