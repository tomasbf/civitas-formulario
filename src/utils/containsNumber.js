export default function containsNumber(str) {
    return /\d/.test(str);
      
}