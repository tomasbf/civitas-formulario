import precoView from "../views/PrecoView.js";

const botaoServico = document.querySelector("#servico");
const botaoProduto = document.querySelector("#produto");
const botaoCliente = document.querySelector("#cliente");
const botaoAnunciante = document.querySelector("#anunciante");

const clienteCabecalho = document.querySelector("label[for=\"cliente\"] p");
const anuncianteCabecalho = document.querySelector("label[for=\"anunciante\"] p");

const tituloCabecalho = document.querySelector("label[for=\"titulo\"] span");

botaoProduto.addEventListener("click", () => {

    botaoProduto.setAttribute("checked", "");
    botaoServico.removeAttribute("checked");


    tituloCabecalho.textContent = "Nome";
    anuncianteCabecalho.textContent = "Venda";
    botaoAnunciante.setAttribute("value", "Venda");

    clienteCabecalho.textContent = "Compra";
    botaoCliente.setAttribute("value", "Compra");


    precoView(true);


});

botaoServico.addEventListener("click", () => {

    botaoProduto.removeAttribute("checked");
    botaoServico.setAttribute("checked", "");

    tituloCabecalho.textContent = "Título";

    anuncianteCabecalho.textContent = "Prestação";
    botaoAnunciante.setAttribute("value", "Prestação");

    clienteCabecalho.textContent = "Contratação";
    botaoCliente.setAttribute("value", "Contratação");

    precoView(false);

});

botaoCliente.addEventListener("click", () => {

    botaoAnunciante.removeAttribute("checked");
    botaoCliente.setAttribute("checked", "");


});

botaoAnunciante.addEventListener("click", () => {

    botaoCliente.removeAttribute("checked");
    botaoAnunciante.setAttribute("checked", "");

    
});



