import popupMsg from "../utils/popupMsg.js";
import getPessoa from "../controllers/getPessoa.js";
const btn = document.querySelector(".forms button");
const senha = document.querySelector("#senha");
const email = document.querySelector("#email");

btn.addEventListener("click", async(event) => {
    event.preventDefault();

    const resposta = await getPessoa(email.value, null);
    
    if(resposta === null) {
        popupMsg(`label[for=\"email\"]`, "Email inválido");
        return false;
    }

    const pessoa = await resposta[0];
    const tipo = await resposta[1];

    if(pessoa !== null && pessoa.senha === senha.value) {
        
        document.location.href = `./perfil.html?id=${pessoa.id}&tipo=${tipo}`;
        return true;

    };

    if(pessoa === null) {
        popupMsg(`label[for=\"email\"]`, "Email inválido");
    } else if (pessoa.senha !== senha.value) {
        
        console.log(pessoa.senha);
        popupMsg(`label[for=\"senha\"]`, "Senha inválida");
    }
    return false;



});

