import getServicosProdutosId from "../controllers/getServicosProdutosId.js";
import mostrarPessoa from "../controllers/mostrarPessoa.js";
import mostrarServicoProduto from "./mostrarServicoProduto.js";



const anunciarBtn = document.querySelector(".conta a");
const informacoesPerfil = document.querySelector("#informacoes");
const produtos = document.querySelector(".produtos_anunciados");
const servicos = document.querySelector(".servicos_anunciados");


let params = (new URL(document.location.href)).searchParams;
const id = params.get('id');
const tipo = params.get('tipo');

if(id === null || (tipo !== "fisica" && tipo !== "juridica")) {
    
    document.location.href='./entrar.html';
}

mostrarPessoa(informacoesPerfil, id, tipo);

mostrarServicoProduto(id, "servicos", servicos, tipo);
mostrarServicoProduto(id, "produtos", produtos, tipo);


anunciarBtn.addEventListener("click", (e) => {
    e.preventDefault();

    document.location.href = `./anunciar.html?id=${id}&tipo=${tipo}`;

});










