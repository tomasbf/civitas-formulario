import getServicosProdutosId from "../controllers/getServicosProdutosId.js";
import servicoListaView from "../views/ServicoListaView.js";
import produtoListaView from "../views/ProdutoListaView.js";


export default async function mostrarServicoProduto (id, qual, listaHTML, tipo, comNome=false){
    const resultados = await getServicosProdutosId(id, qual, tipo);

    switch(qual) {
        case "servicos":
            await listaHTML.insertAdjacentHTML("beforeend", await servicoListaView(resultados, comNome));
            return true;

        case "produtos":

            await listaHTML.insertAdjacentHTML("beforeend", await produtoListaView(resultados, comNome));
            return true;

        default:
            return false;
    }

}