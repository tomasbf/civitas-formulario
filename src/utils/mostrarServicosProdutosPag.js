import mostrarServicoProduto from "./mostrarServicoProduto.js";
const lista = document.querySelector(".caixa_exemplos ul");
const buscar = document.querySelector("#buscar_servico");

let excluidos = document.createElement("ul");

const url = new URL(document.location.href);

if(url.pathname.includes("servico")) {
    mostrarServicoProduto(null, "servicos", lista, null, true);

} else if (url.pathname.includes("produto")) {
    mostrarServicoProduto(null, "produtos", lista, null, true);

}


const filtrarLista = (e) => {

    const digitado = buscar.value;

    let children = lista.children; 
    console.log(excluidos.children.length);
    
    for(let i = 0; i < excluidos.children.length; i++) {
        let titulo = excluidos.children.item(i).children.item(0).children.item(0).children.item(2).children.item(1).textContent;
        
        if(titulo.includes(digitado)) {
            lista.appendChild(excluidos.removeChild(excluidos.children.item(i)));

        }
    }

    for(let i = 0; i < children.length; i++) {
        let titulo = children.item(i).children.item(0).children.item(0).children.item(2).children.item(1).textContent;
        

        if(!titulo.includes(digitado)) {
            excluidos.appendChild(lista.removeChild(children.item(i)));
        } 

    }

}

buscar.addEventListener("search", filtrarLista);


