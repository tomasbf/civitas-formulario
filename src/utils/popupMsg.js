import colocarOutline from "./colocarOutline.js";
import popupRemove from "./popupRemove.js";

function popupMsg(location, msg, remove=false) {

    const popup_location = document.querySelector(location);
    const input = document.querySelectorAll(location + " input");
    let popup = document.querySelector(location + " .popuptext");

    if(remove === true) {

        popupRemove(popup, input);
        return;
    }


    if(popup === null) {
        popup = document.createElement("span");
        popup.className = "popuptext";
        popup_location.appendChild(popup);
        
        
        popup.addEventListener("click", async(event) => {
            popupRemove(event.target, input);
        });

    }
    popup.textContent = msg;
    colocarOutline(input);


    if(!popup.hasAttribute("style")) {
        popup.setAttribute("style", 
            `visibility: visible;
            -webkit-animation: fadeIn 1s;
            animation: fadeIn 1s;
        `);
    }

}
export default popupMsg;
