import removerOutline from "./removerOutline.js";

export default async function popupRemove(popup, input) {


    if(popup !== null && popup.hasAttribute("style")) {

        for (let i = 0; i < input.length; i++) {
            if(input[i] !== null && input[i] !== undefined) {
                removerOutline(input);
            }
        }

        popup.removeAttribute("style");
    }


}