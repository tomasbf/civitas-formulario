const formFisica = document.querySelector(".pessoa_fisica");
const formJuridica = document.querySelector(".pessoa_juridica");

const escolherFormulario = document.querySelector("#escolher_tipo_usuario");

const selecionarFisica = document.querySelector("#escolher_fisica");
const selecionarJuridica = document.querySelector("#escolher_juridica");

const checarFormulario = async () => {
    if(selecionarFisica.checked) {
        formFisica.setAttribute("style", "opacity: 1; position: relative; z-index: 1;");
        formJuridica.setAttribute("style", "opacity: 0; position: absolute; z-index: -1;");

    } else {
        formFisica.setAttribute("style", "opacity: 0; position: absolute; z-index: -1;");
        formJuridica.setAttribute("style", "opacity: 1; position: relative; z-index: 1;");
    }
};

(async () => {
    checarFormulario();
})();


selecionarFisica.addEventListener("click", async() => { 
    selecionarJuridica.removeAttribute("checked");
    selecionarFisica.setAttribute("checked", "");
    await checarFormulario();
 });
selecionarJuridica.addEventListener("click", async() => { 
    selecionarFisica.removeAttribute("checked");
    selecionarJuridica.setAttribute("checked", "");
    await checarFormulario(); 
});










