const mostrarFisica = (data) => {

    const dataNascimento = data.dataNascimento.split("-");
    const ano = dataNascimento[0];
    const mes = dataNascimento[1];
    const dia = dataNascimento[2];




    return `
    <tr>
        <th>Nome:</th>
        <td id=\"nome\">${data.nome}</td>
    </tr>
    <tr>
        <th>Email:</th>
        <td id=\"email\">${data.email}</td>
    </tr>
    <tr>
        <th>CPF:</th>
        <td id=\"cpf\">${data.cpf}</td>
    </tr>
    <tr>
        <th>Data de nascimento:</th>
        <td id=\"data_nascimento\">${dia}/${mes}/${ano}</td>
    </tr>
    <tr>
        <th>Senha:</th>
        <td><a href=\"trocar_senha.html\">Trocar senha</a></td>
    </tr>
    `;
};

export default mostrarFisica;