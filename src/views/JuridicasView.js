const mostrarJuridica = (data) => {
    return `
    <tr>
        <th>Nome:</th>
        <td id="nome">${data.nome}</td>
    </tr>
    <tr>
        <th>Email:</th>
        <td id="email">${data.email}</td>
    </tr>
    <tr>
        <th>CNPJ:</th>
        <td id="cnpj">${data.cnpj}</td>
    </tr>
    <tr>
        <th>CEP:</th>
        <td id="cep">${data.cep}</td>
    </tr>
    <tr>
        <th>Complemento:</th>
        <td id="complemento">${data.complemento}</td>
    </tr>
    <tr>
        <th>Número:</th>
        <td id="numero">${data.numero}</td>
    </tr>
    <tr>
        <th>Nome do representante:</th>
        <td id="nome_representante">${data.nome_representante}</td>
    </tr>
    <tr>
        <th>Senha:</th>
        <td><a href="trocar_senha.html">Trocar senha</a></td>
    </tr>
    `;
};

export default mostrarJuridica;