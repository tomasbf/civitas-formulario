export default function precoView(ehProduto) {
    const labelPreco = document.querySelector("label[for=\"preco\"]");
    switch(ehProduto) {
        case true:
            
            labelPreco.innerHTML =  `Preço
                <div class="preco">
                    <input class="moeda" type="text" value="R$" readonly>
                    <input type="number" step="any" name="preco" id="preco" min="0" required>
                </div>
            `;
            break;

        case false:

            labelPreco.innerHTML =  ` Preço
                                    <input type="text" name="preco" id="preco" required>
                                    `;

            break;
    }


}