import getPessoaId from "../controllers/getPessoaId.js";
const produtoListaView = async (produtos, comNome = false) => {

    if (produtos.length === 0) return "<p>Não há produtos para mostrar</p>";

    if(!comNome) {
        let lista = ``;
        produtos.map((e) => {
            lista = lista.concat(

                `
                <li>
                    <table>
                        <tr>
                            <th>Nome:</th>
                            <td>${e.titulo}</td>
                        </tr>
                        <tr>
                            <th>Descrição:</th>
                            <td>${e.descricao}</td>
                        </tr>
                        <tr>
                            <th>Preço:</th>
                            <td>${e.preco}</td>
                        </tr>
                        <tr>
                            <th>Categoria:</th>
                            <td>${e.categoria}</td>
                        </tr>
                    </table>
                </li>
                `

            );
        });

        return lista;
    }
    else {

        let lista = ``;

        for(let i = 0; i < produtos.length; i++) {
            let classe;
            if (i % 2) classe = "class=\"a_direita\"";
            else classe = "class=\"a_esquerda\"";


            const pessoa = await getPessoaId(produtos[i].idPessoa, produtos[i].tipoPessoa);

            lista = lista.concat(

                `
                <li ${classe}>
                `);

            lista = lista.concat(`
                    <table>
                        <tr>
                            <th>Usuário:</th>
                            <td>${await pessoa.nome}</td>
                        </tr>
                        <tr>
                            <th>Tipo:</th>
                            <td>Produto</td>
                        </tr>                        
                        <tr>
                            <th>Nome:</th>
                            <td class=\"titulo\">${produtos[i].titulo}</td>
                        </tr>
                        <tr>
                            <th>Descrição:</th>
                            <td>${produtos[i].descricao}</td>
                        </tr>
                        <tr>
                            <th>Preço:</th>
                            <td>${produtos[i].preco}</td>
                        </tr>
                        <tr>
                            <th>Categoria:</th>
                            <td>${produtos[i].categoria}</td>
                        </tr>
                    </table>
                </li>
                `

            );

        }

    

        return lista;
    }

    

};
export default produtoListaView;