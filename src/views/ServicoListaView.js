import getPessoaId from "../controllers/getPessoaId.js";
const servicoListaView = async (servicos, comNome = false) => {

    if (servicos.length === 0) return "<p>Não há serviços para mostrar</p>";
    if (!comNome) {
        let lista = ``;
        servicos.map((e) => {
            lista = lista.concat(

                `
            <li>
                <table>
                    <tr>
                        <th>Título:</th>
                        <td>${e.titulo}</td>
                    </tr>
                    <tr>
                        <th>Descrição:</th>
                        <td>${e.descricao}</td>
                    </tr>
                    <tr>
                        <th>Preço:</th>
                        <td>${e.preco}</td>
                    </tr>
                    <tr>
                        <th>Categoria:</th>
                        <td>${e.categoria}</td>
                    </tr>
                </table>
            </li>
            `

            );
        });

        return lista;
    } else {


        let lista = ``;

        for(let i = 0; i < servicos.length; i++) {

            let classe;
            if (i % 2) classe = "class=\"a_direita\"";
            else classe = "class=\"a_esquerda\"";


            const pessoa = await getPessoaId(servicos[i].idPessoa, servicos[i].tipoPessoa);

            lista = lista.concat(

                `
                <li ${classe}>
                `);

            lista = lista.concat(

                `
                <table>
                    <tr>
                        <th>Usuário:</th>
                        <td>${await pessoa.nome}</td>
                    </tr>
                    <tr>
                        <th>Tipo:</th>
                        <td>Serviço</td>
                    </tr>
                    <tr>
                        <th>Título:</th>
                        <td class=\"titulo\">${servicos[i].titulo}</td>
                    </tr>
                    <tr>
                        <th>Descrição:</th>
                        <td>${servicos[i].descricao}</td>
                    </tr>
                    <tr>
                        <th>Preço:</th>
                        <td>${servicos[i].preco}</td>
                    </tr>
                    <tr>
                        <th>Categoria:</th>
                        <td>${servicos[i].categoria}</td>
                    </tr>
                </table>
            </li>
            `

            );



        }

       
        return lista;


    }



};
export default servicoListaView;